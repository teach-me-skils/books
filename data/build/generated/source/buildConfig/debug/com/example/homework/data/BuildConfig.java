/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.homework.data;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.example.homework.data";
  public static final String BUILD_TYPE = "debug";
}
