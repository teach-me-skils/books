package com.example.data

import com.example.domain.models.FirstItemBook
import com.example.domain.models.SecondItemBook
import com.example.domain.models.ThirdItemBook
import com.example.domain.repository.BookRepository

class BookRepositoryImpl() : BookRepository {

    override fun listOfBook(bookModels: Unit?): List<Any> {
        return listOf(
            FirstItemBook("1"),
            SecondItemBook("2"),
            ThirdItemBook("3"),
            FirstItemBook("1"),
            SecondItemBook("2"),
            ThirdItemBook("3"),
            FirstItemBook("1"),
            SecondItemBook("2"),
            ThirdItemBook("3"),
            FirstItemBook("1"),
            SecondItemBook("2"),
            ThirdItemBook("3"),
            FirstItemBook("1"),
            SecondItemBook("2"),
            ThirdItemBook("3"),
            FirstItemBook("1"),
            SecondItemBook("2"),
            ThirdItemBook("3"),
        )
    }
}
