package com.example.homework.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.homework.bace.BaseFragment
import com.example.data.BookRepositoryImpl
import com.example.homework.databinding.FragmentMainBinding
import com.example.domain.repository.BookRepository
import com.example.domain.useCase.BookUseCase
import com.example.homework.list.MyAdapter
import com.example.homework.myBook.BookViewModel

class MainFragment : BaseFragment<FragmentMainBinding>() {

    private val bookRepositoryImpl: BookRepository = BookRepositoryImpl()
    private val bookUseCase = BookUseCase(bookRepositoryImpl)


    private lateinit var viewModel: BookViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = BookViewModel(bookUseCase)
    }

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMainBinding = FragmentMainBinding.inflate(inflater, container, false)

    override fun FragmentMainBinding.onBindView(saveInstanceState: Bundle?) {

        val myAdapter = MyAdapter()

        viewModel.viewBook(viewModel)
        viewModel.bookViewLiveData.observe(viewLifecycleOwner) {
            myAdapter.submitList(
                viewModel.bookViewLiveData.value
            )
            list.adapter = myAdapter
        }
    }
}