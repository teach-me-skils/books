package com.example.homework.myBook

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.useCase.BookUseCase

class BookViewModel(private val bookUseCase: BookUseCase) : ViewModel() {

    private val _bookViewLiveData = MutableLiveData<List<Any>>()
    val bookViewLiveData: LiveData<List<Any>> = _bookViewLiveData

    fun viewBook(model: BookViewModel?){
        val result = bookUseCase.execute()
        _bookViewLiveData.value = result
    }
}