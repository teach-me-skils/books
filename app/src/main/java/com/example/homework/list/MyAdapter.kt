package com.example.homework.list


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.example.domain.models.FirstItemBook
import com.example.domain.models.SecondItemBook
import com.example.domain.models.ThirdItemBook
import com.example.homework.bace.BaseViewHolder
import com.example.homework.databinding.ItemFirstBookBinding
import com.example.homework.databinding.ItemSecondBookBinding
import com.example.homework.databinding.ItemThirdBookBinding

class MyAdapter : ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(
    object : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is FirstItemBook && newItem is FirstItemBook -> oldItem == newItem
            oldItem is SecondItemBook && newItem is SecondItemBook -> oldItem == newItem
            oldItem is ThirdItemBook && newItem is ThirdItemBook -> oldItem == newItem
            else -> false
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is FirstItemBook && newItem is FirstItemBook -> oldItem.id == newItem.id
            oldItem is SecondItemBook && newItem is SecondItemBook -> oldItem.id == newItem.id
            oldItem is ThirdItemBook && newItem is ThirdItemBook -> oldItem.id == newItem.id
            else -> false
        }

    }
) {
    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is FirstItemBook -> FIRST_BOOK
        is SecondItemBook -> SECOND_BOOK
        is ThirdItemBook -> THIRD_BOOK
        else -> throw IllegalArgumentException(
            "MyAdapter can't handle the item with the type + ${
                getItem(
                    position
                )
            }"
        )
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        FIRST_BOOK -> FirstItemViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        SECOND_BOOK -> SecondItemViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        THIRD_BOOK -> ThirdItemViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw IllegalArgumentException("MyAdapter can't handle item with type $viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) =
        holder.handleItem(getItem(position))

    companion object {
        private const val FIRST_BOOK = 999
        private const val SECOND_BOOK = 998
        private const val THIRD_BOOK = 997
    }
}
private class FirstItemViewHolder(parent: ViewGroup) :
    BaseViewHolder<ItemFirstBookBinding, FirstItemBook>(
        ItemFirstBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) {
    override fun ItemFirstBookBinding.bind(value: FirstItemBook) {
    }
}

private class SecondItemViewHolder(parent: ViewGroup) :
    BaseViewHolder<ItemSecondBookBinding, SecondItemBook>(
        ItemSecondBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) {
    override fun ItemSecondBookBinding.bind(value: SecondItemBook) {
    }
}

private class ThirdItemViewHolder(parent: ViewGroup) :
    BaseViewHolder<ItemThirdBookBinding, ThirdItemBook>(
        ItemThirdBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) {
    override fun ItemThirdBookBinding.bind(value: ThirdItemBook) {
    }
}