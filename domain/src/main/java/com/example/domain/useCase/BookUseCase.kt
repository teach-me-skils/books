package com.example.domain.useCase

import com.example.domain.base.UseCase
import com.example.domain.repository.BookRepository

class BookUseCase(private val bookRepository: BookRepository) : UseCase<Unit, List<Any>> {
    override fun execute(param: Unit?): List<Any> {
        return bookRepository.listOfBook(param)
    }
}